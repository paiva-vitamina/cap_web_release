if ($('main').hasClass('dashboard')) {
    $('.cap-symbol').hide();
    $('header').addClass('header-dashboard');
    $('footer').addClass('footer-dashboard');
}

$(window).on('load', function () {

    setTimeout(function(){
        $('.loader').fadeOut();
    }, 1000);

});


$(document).ready(function() {


	// setTimeout(function(){
    //     $('body').addClass('visible');
    // }, 800);

    if ($('.dashboard').hasClass('home')) {
        $('.block-menu .menu li').removeClass('active');
        $('.block-menu .menu .home').addClass('active'); 
    }
    if ($('.dashboard').hasClass('account')) {
        $('.block-menu .menu li').removeClass('active');
        $('.block-menu .menu .account').addClass('active');
    }
    if ($('.dashboard').hasClass('extrato')) {
        $('.block-menu .menu li').removeClass('active');
        $('.block-menu .menu .extrato').addClass('active');
    }
    if ($('.dashboard').hasClass('boleto')) {
        $('.block-menu .menu li').removeClass('active');
        $('.block-menu .menu .boleto').addClass('active');
    }
    if ($('.dashboard').hasClass('servicos')) {
        $('.block-menu .menu li').removeClass('active');
        $('.block-menu .menu .servicos').addClass('active');
    }
    if ($('.dashboard').hasClass('anamnese')) {
        $('.block-menu .menu li').removeClass('active');
        $('.block-menu .menu .anamnese').addClass('active');
    }
    if ($('.dashboard').hasClass('notificacoes')) {
        $('.block-menu .menu li').removeClass('active');
        $('.block-menu .menu .notificacoes').addClass('active');
    }
    if ($('.dashboard').hasClass('config')) {
        $('.block-menu .menu li').removeClass('active');
        $('.block-menu .menu .config').addClass('active');
    }
    if ($('.dashboard').hasClass('perm')) {
        $('.block-menu .menu li').removeClass('active');
        $('.block-menu .menu .perm').addClass('active');
    }


	// Easing Page Scroll


	$(function() {
	    $('a.page-scroll').bind('click', function(event) {
	        var $anchor = $(this);
	        $('html, body').stop().animate({
	            scrollTop: $($anchor.attr('href')).offset().top
	        }, 1500, 'easeInOutExpo');
	        event.preventDefault();
	    });
    });

    jQuery.easing.jswing = jQuery.easing.swing;
    jQuery.extend(jQuery.easing, {
        def: "easeOutQuad",
        easeInOutExpo: function(e, f, a, h, g) {
            if (f == 0) {
                return a
            }
            if (f == g) {
                return a + h
            }
            if ((f /= g / 2) < 1) {
                return h / 2 * Math.pow(2, 10 * (f - 1)) + a
            }
            return h / 2 * (-Math.pow(2, -10 * --f) + 2) + a
        },

    });

	

	// $(window).scroll(function () {
    //     var scroll = $(window).scrollTop();

    //     if (scroll > 250) {
    //         $('.back-to-top').fadeIn();
    //         $('.inner-head').addClass('fixed');
    //     } else {
    //         $('.back-to-top').fadeOut();
    //         $('.inner-head').removeClass('fixed');  
    //     }

    //     if (scroll > 300) {
    //         $('.inner-head').addClass('visible');
    //     } else {
    //         $('.inner-head').removeClass('visible');
    //     }

    // });
    

    // Show Password

    $('.show-pass').click(function () {
        $(this).siblings('.current_password').toggleClass('visible-pass');

        if ($('.current_password').hasClass('visible-pass')) {
            $(this).siblings('.current_password').attr('type', 'text');
        } else {
            $(this).siblings('.current_password').attr('type', 'password');
        }

    });

    // Validator

    $('.form-custom').validator();


    // Custom Radio and Checkbox

    $('.form-custom .radio label').click(function () {
        $('.form-custom .radio label').removeClass('selected');
        $(this).addClass('selected');
    });

    $('.form-custom .checkbox label input').click(function () {
        $(this).parent().toggleClass('selected');
    });


    // Masks

    $('.birth').mask('00 / 00 / 0000');

    $('.cpf').mask('000.000.000-00', { reverse: true });


    $('.custom-cards .block').mousedown(function(){
        $(this).addClass('clicked');
    });

    $('.custom-cards .block').mouseup(function () {
        $(this).removeClass('clicked');
    });


    $('.switch input').on('change', function(){
        $(this).parent().siblings('.label-toggle').toggleClass('active');

        if (this.checked) {
            $(this).parent().siblings('.label-toggle').html('Sim');
        } else {
            $(this).parent().siblings('.label-toggle').html('Não');
        }
    });

    $('.show-block').click(function(){
        $('.hidden-block').slideToggle('fast');
    });


    // Datepicker

    ; (function ($) {
        $.fn.datepicker.dates['pt-BR'] = {
            days: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
            daysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
            daysMin: ["D", "S", "T", "Q", "Q", "S", "S"],
            months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            monthsShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
            today: "Hoje",
            monthsTitle: "Meses",
            clear: "Limpar",
            format: "dd.mm.yyyy"
        };
    }(jQuery));


    $('.input-daterange').datepicker({
        language: "pt-BR",
        orientation: "bottom center",
        autoclose: true,
    });


    $('.btn-menu').click(function () {
        $(this).toggleClass('active');
        $(this).find('.menu-trigger').toggleClass('active');
        $('.block-menu').find('.wrap').toggleClass('active');
        $('.block-menu').toggleClass('menu-open');
        $('body').toggleClass('not-scroll');

    });


	
	
    
	

	
	






});